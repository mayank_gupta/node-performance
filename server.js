var restify = require('restify');
var mongo = require('mongoskin');

var dbURL = 'mongodb://localhost:27017/fame_test_db';
var coll = 'test';

function forkServer () {
  var db = mongo.db(dbURL, {safe: true});
  var server = restify.createServer();
  server.use(restify.gzipResponse());
  server.get('/ping', function (request, response, next) {
    response.send('pong');
  });
  server.get('/ping/db', function (request, response, next) {
    db.collection(coll).findOne(function (err, data) {
      response.send(data);
    });
  });
  server.listen(8888, function () {});
}

function clusteredServer ( num) {

  var cluster = require('cluster');
  var numCPUs = parseInt(num);
  if (!numCPUs || isNaN(numCPUs)) {
    numCPUs = require('os').cpus().length;
  }
  else {
    numCPUs--;
  }

  if (cluster.isMaster) {
  
    for(var i =0; i < numCPUs; i++) {
      cluster.fork();
    }
    cluster.on('exit', function (worker, code, signal) {
      console.log('worker ' + worker.process.pid + ' died');
    });
  }
  else {
    forkServer();
  }
}

function singleServer () {
  forkServer();
}

function main () {
  var mode = process.argv[2];

  switch(mode) {
    case 'cluster': 
      clusteredServer(process.argv[3]);
      break;
    case 'single':
      singleServer();
      break;
    default: 
      console.log('Invalid argument', 'Options', 'node server.js single', 'node server.js cluster');
      process.exit(1);
      break;
  }
}

main();
